import React from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom"
import CreateProject from "./Projects/CreateProject"
import ProjectMainPage from "./Projects/ProjectMainPage"
import ShowAllProjects from "./Projects/ShowAllProjects"
import CreateIssue from "./Issues/CreateIssue"
import IssueMainPage from "./Issues/IssueMainPage"
import ShowAllIssues from "./Issues/ShowAllIssues"
import TraceIssue from "./Issues/TraceIssue"
import CommentMainPage from "./Comments/CommentMainPage"
import ShowAllComment from "./Comments/ShowAllComment"
import CreateComment from "./Comments/CreateComment"
import TraceProject from './Projects/TraceProject'
import TraceComment from './Comments/TraceComment'



function App(props) {

  return (
    <div className = 'app'>
      <h1>Project Issue Tracker!</h1>
      <Route path='/'><ProjectMainPage /></Route>
      <Route  path = '/projects/:projectId/issues' component = {IssueMainPage} />
      <Route path='/projects/:projectId/issues/allissues/:issueId/comments' component={CommentMainPage} />
      
      <Switch>
        <Route exact path ='/projects/:projectId/issues/allissues/:issueId/comments/allcomments/:commentId'  component={TraceComment} />
        <Route path ='/projects/:projectId/issues/allissues/:issueId/comments/allcomments' component={ShowAllComment} />
        <Route path = "/projects/:projectId/issues/allissues/:issueId/comments/createcomment"component={CreateComment}/>

        <Route exact path = '/projects/:projectId/issues/allissues/:issueId' component={TraceIssue} />
        <Route exact path = '/projects/:projectId/issues/allissues' component={ShowAllIssues} />
        <Route exact path = "/projects/:projectId/issues/createissue" component={CreateIssue} />

        <Route exact path = '/projects/:projectId' component={TraceProject} />
        <Route exact path = '/projects'><ShowAllProjects /></Route>
        <Route exact path = '/createproject'><CreateProject /></Route>
      </Switch>        

    </div>
  );
}

export default App;
