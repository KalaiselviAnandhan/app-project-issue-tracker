import React from "react"
import {Link} from "react-router-dom"

class CommentMainPage extends React.Component{
    
    render(){

            return(
                <div className = 'comment-container'>

                    <Link to= {`/projects/${this.props.match.params.projectId}/issues/allissues/${this.props.match.params.issueId}/comments`}>
                        <button value = {0} >CommentHome</button>
                    </Link>
                    <Link to={`/projects/${this.props.match.params.projectId}/issues/allissues/${this.props.match.params.issueId}/comments/allcomments`}>
                        <button value = {1} >Show All Comments</button>
                    </Link>
                    <Link to={`/projects/${this.props.match.params.projectId}/issues/allissues/${this.props.match.params.issueId}/comments/createcomment`}>
                        <button value = {2} >Create Comment</button>
                    </Link>
                </div>
            )
    }
}
export default CommentMainPage