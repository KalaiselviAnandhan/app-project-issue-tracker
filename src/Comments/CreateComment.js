import React from 'react'
import "../CSS/CreateComment.css"
import axios from "axios"


class CreateComment extends React.Component {

    constructor(){
        super()
        this.state = {
            comment_id:'',
            user_name:'',
            comment:''
        }
    }

    handleOnChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleOnSubmit = (event)=>{
        event.preventDefault()
        const formdata =this.state

        axios.get(`https://kalaianand-project-issue-track.herokuapp.com/checkvalid/comment/${this.props.match.params.projectId}/${this.props.match.params.issueId}/${formdata.comment_id}`)
        .then((response)=>{
            const postUrl = `https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${this.props.match.params.issueId}/comments`
            return axios.post(postUrl,formdata)
        })
        .then((res)=>{
            console.log(res)
            alert("Comment Created..")
        })
        .catch(error=>{
            console.log(error)
        })
    }
    render(){
        return (
            <form onSubmit={this.handleOnSubmit}>
                <div className="form-box">
                    <label>Id</label>
                    <input type="number" placeholder="Comment Id" name="comment_id" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>User Name</label>
                    <input type="text" placeholder="User Name" name="user_name" required onChange={this.handleOnChange}/>
                </div>

                <div className="form-box">
                    <label>Comment</label>
                    <textarea placeholder="Comment" name="comment" row="20" required onChange={this.handleOnChange}/>
                </div>

                <div className="form-box"> 
                    <button type="submit" >Create</button>
                </div>
            </form>
        )
    }
}
export default CreateComment