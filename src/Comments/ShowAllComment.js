import React from "react"
import axios from "axios"
import TableData from "./TableData"

class ShowAllComment extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            comments: []
        }
    }
    
    fetchData = ()=>{
        const getURL =  `https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${this.props.match.params.issueId}/comments`
        axios.get(getURL)
        .then(response => {
            this.setState({
                comments : response.data
            })
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
        this.fetchData()
    }

    handleDelete=(event)=>{
        let commentId = event.target.id
        axios.delete(`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${this.props.match.params.issueId}/comments/${commentId}`)
        .then((response) => {
            console.log(response)
            this.fetchData()
            alert("Comment Deleted..")
        })
        .catch(error => console.log(error))
    }

    render(){

        return(
                <table>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Username</td>
                            <td>Comment</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.comments.map((items,index)=><TableData projectid={this.props.match.params.projectId} issueid = {this.props.match.params.issueId} commentData = {items} key={index} clickDelete = {this.handleDelete}/>)}
                    </tbody>
                </table>
        )
    }
}
export default ShowAllComment