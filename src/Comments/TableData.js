import React from "react"
import {Link} from "react-router-dom"

class TableData extends React.Component{
    render(){
        let id=this.props.commentData.comment_id
        return(
        <tr>
            <td>
                {this.props.commentData.comment_id}
            </td>
            <td>
                {this.props.commentData.user_name}
            </td>
            <td>
                {this.props.commentData.comment}
            </td>
            <td>
                <Link to ={`/projects/${this.props.projectid}/issues/allissues/${this.props.issueid}/comments/allcomments/${id}`}>
                    <input type = "button" value ='edit'/>
                </Link>
            </td>
            <td>
                <input type='button' id = {this.props.commentData.comment_id} onClick = {(event)=>this.props.clickDelete(event,this.props.commentData)}value='delete'/>

            </td>
        </tr>
        )
    }
}
export default TableData