import React from 'react'
import axios from 'axios'

class TraceComment extends React.Component {

    constructor(){
        super()
        this.state = {
            user_name:'',
            comment:''
        }
    }

    handleOnChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleOnSubmit = (event)=>{
        event.preventDefault()
        const data =this.state
        axios.put(`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${this.props.match.params.issueId}/comments/${this.props.match.params.commentId}`,data)
        .then(response=>{
            console.log(response)
            alert("Comment Updated...")
        })
        .catch(error=>console.log(error))
        return false;
    }

    render(){

        return (
            <form onSubmit= {this.handleOnSubmit}>
                <div className="form-box">
                    <label>User Name</label>
                    <input type="text" placeholder="User Name" name="user_name" required onChange={this.handleOnChange}/>
                </div>

                <div className="form-box">
                    <label>Comment</label>
                    <textarea placeholder="Comment" name="comment" row="20" required onChange={this.handleOnChange}/>
                </div>
    
                <div className="form-box"> 
                    <button type="submit" >Submit</button>
                </div>
            </form>
        )
    }
}
export default TraceComment