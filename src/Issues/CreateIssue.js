import React from 'react'
import "../CSS/CreateIssue.css"
import axios from "axios"

class CreateIssue extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            issue_id:'',
            issue_title:'',
            username:'',
            issue_description:'',
            issue_status:'',
            issue_severity:'',
            issue_classification:''
        }
    }

    handleOnChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleOnSubmit = (event)=>{
        event.preventDefault()
        const formdata =this.state
        const issueId = formdata.issue_id
        axios.get(`https://kalaianand-project-issue-track.herokuapp.com/checkvalid/issue/${this.props.match.params.projectId}/${issueId}`)
        .then((res)=>{
            const posturl=`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues`
            return axios.post(posturl,formdata)
        })
        .then((res)=>{
            console.log(res)
            alert("Issue Created..")
        })
        .catch(error=>{
            console.log("error",error)
            alert("Please enter the valid Id")
        })
    }

    render(){
        return (
            <form onSubmit={this.handleOnSubmit}>
                <div className="form-box">
                    <label>Issue Id</label>
                    <input type="number" placeholder="Issue Id" name="issue_id" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Issue Title</label>
                    <input type="text" placeholder="Issue Title" name="issue_title" required onChange={this.handleOnChange}/>
                </div>

                <div className="form-box">
                    <label>Username</label>
                    <input type="text" placeholder="Username" name="username" required onChange={this.handleOnChange}/>
                </div>

                <div className="form-box">
                    <label>Issue Description</label>
                    <input type="text" placeholder="Issue Description" name="issue_description" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Status</label>
                    <select name="issue_status" required onChange={this.handleOnChange}>
                        <option value="" >none</option>
                        <option value="open">open</option>
                        <option value="close">close</option>
                        <option value="progress">progress</option>
                    </select>
                </div>

                <div className="form-box">
                    <label>Issue Severity</label>
                    <select name="issue_severity" onChange={this.handleOnChange} required>
                        <option value="" >none</option>
                        <option value="minor" >minor</option>
                        <option value="major">major</option>
                        <option value="critical">critical</option>
                    </select>
                </div>

                <div className="form-box">
                    <label>Classification</label>
                    <select name="issue_classification" onChange={this.handleOnChange} required>
                        <option value="" >none</option>
                        <option value="security" >security</option>
                        <option value="crash">crash</option>
                        <option value="UI/Usability">UI/Usability</option>
                        <option value="other">other</option>
                    </select>
                </div>

                <div className="form-box"> 
                    <button type="submit" >Create</button>
                </div>
            </form>
        )
    }
}
export default CreateIssue