import React from "react"
import {Link} from "react-router-dom"

class IssueMainPage extends React.Component{
    
    render(){

            return(
                <div className = 'issue-container'>
                    <Link to = {`/projects/${this.props.match.params.projectId}/issues`}>
                        <button value = {0} >IssueHome</button>
                    </Link>
                    
                    <Link to = {`/projects/${this.props.match.params.projectId}/issues/allissues`}>
                        <button value = {1} >Show All Issues</button>
                    </Link>
                    <Link to = {`/projects/${this.props.match.params.projectId}/issues/createissue`}>
                        <button value = {2} >Create Issue</button>
                    </Link>
                </div>
            )
    }
}
export default IssueMainPage