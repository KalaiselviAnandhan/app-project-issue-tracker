import React from "react"
import axios from "axios"
import Tabledata from "./Tabledata"


class ShowAllIssues extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            issues : [],
            checkStatus : ""
        }
    }

    fetchData = ()=>{
        const getURL = `https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues`
        axios.get(getURL)
        .then(response => {
            this.setState({
                issues : response.data,
                checkStatus : ""
            })
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
        this.fetchData()
    }

    handleDelete=(event)=>{
        let issueId = event.target.id
        axios.delete(`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${issueId}`)
        .then(response => {
            alert("Issue Deleted...")
            this.fetchData()
            console.log(response)
        })
        .catch(error => console.log(error))
    }

    render(){
            return(
                <table>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Issue Name</td>
                            <td>Username</td>
                            <td>Issue Description</td>
                            <td>Issue Status</td>
                            <td>Issue Severity</td>
                            <td>Issue Classification</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.issues.map((items,index)=><Tabledata projectid={this.props.match.params.projectId}  issueData = {items} key={index}  clickDelete = {this.handleDelete}/>)}
                    </tbody>
                </table>
            )
        
    }
}
export default ShowAllIssues