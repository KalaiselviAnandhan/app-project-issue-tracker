import React from "react"
import {Link} from "react-router-dom"

class Tabledata extends React.Component{
    
    render(){
        let id=this.props.issueData.issue_id
        return(
        <tr>
            <td>
                {this.props.issueData.issue_id}
            </td>
            <td>
                <Link to ={`/projects/${this.props.projectid}/issues/allissues/${id}/comments`}>
                    <p >
                        {this.props.issueData.issue_title}
                    </p>
                </Link>
            </td>
            <td>
                {this.props.issueData.username}
            </td>
            <td>
                {this.props.issueData.issue_description}
            </td>
            <td>
                {this.props.issueData.issue_status}
            </td>
            <td>
                {this.props.issueData.issue_severity}
            </td>
            <td>
                {this.props.issueData.issue_classification}
            </td>
            <td>
                <Link to ={`/projects/${this.props.projectid}/issues/allissues/${id}`}>
                    <input type = "button" value ='edit'/>
                </Link>
            </td>
            <td>
                <input type="button" id = {this.props.issueData.issue_id} onClick = {(event)=>this.props.clickDelete(event,this.props.issueData)} value='delete' />
            </td>
        </tr>
        )
    }
}
export default Tabledata