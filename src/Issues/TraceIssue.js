import React from 'react'
import axios from "axios"

class TraceIssue extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            issue_title:'',
            username:'',
            issue_description:'',
            issue_status:'',
            issue_severity:'',
            issue_classification:''
        }
    }

    handleOnChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleOnSubmit = (event)=>{
        event.preventDefault()
        const data =this.state
        axios.put(`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}/issues/${this.props.match.params.issueId}`,data)
        .then(response=>{
            console.log(response)
            alert("Issue Updated..")
        })
        .catch(error=>console.log(error))
    }

    render(){

        return (
            <form id = 'form' onSubmit={this.handleOnSubmit}>
    
                <div className="form-box">
                    <label>Issue Title</label>
                    <input type="text" placeholder="Issue Title" name="issue_title" required onChange={this.handleOnChange}/>
                </div>
    
                <div className="form-box">
                    <label>Username</label>
                    <input type="text" placeholder="Username" name="username" required onChange={this.handleOnChange}/>
                </div>
    
                <div className="form-box">
                    <label>Issue Description</label>
                    <input type="text" placeholder="Issue Description" name="issue_description" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Status</label>
                    <select name="issue_status" required onChange={this.handleOnChange}>
                        <option value="" >none</option>
                        <option value="open">open</option>
                        <option value="close">close</option>
                        <option value="progress">progress</option>
                    </select>
                </div>
    
                <div className="form-box">
                    <label>Issue Severity</label>
                    <select name="issue_severity" onChange={this.handleOnChange} required>
                        <option value="" >none</option>
                        <option value="minor" >minor</option>
                        <option value="major">major</option>
                        <option value="critical">critical</option>
                    </select>
                </div>
    
                <div className="form-box">
                    <label>Classification</label>
                    <select name="issue_classification" onChange={this.handleOnChange} required>
                        <option value="" >none</option>
                        <option value="security" >security</option>
                        <option value="crash">crash</option>
                        <option value="UI/Usability">UI/Usability</option>
                        <option value="other">other</option>
                    </select>
                </div>
    
                <div className="form-box"> 
                    <button type="submit" >Submit</button>
                </div>
            </form>
        );
    }
}
export default TraceIssue