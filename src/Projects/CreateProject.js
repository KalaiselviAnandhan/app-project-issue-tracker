import React from 'react'
import "../CSS/CreateProject.css"
import axios from "axios"


class CreateProject extends React.Component {

    constructor(){
        super()
        this.state = {
            project_id:'',
            project_name:'',
            project_description:'',
            project_owner:''
        }
    }

    handleOnChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleOnSubmit = (event)=>{
        event.preventDefault()
        const formdata =this.state
        let projectId = formdata.project_id
        

        axios.get(`https://kalaianand-project-issue-track.herokuapp.com/checkvalid/project/${projectId}`)
        .then((res)=>{
            return axios.post('https://kalaianand-project-issue-track.herokuapp.com/projects',formdata)
        })
        .then((res)=>{
            console.log(res)
            alert("Project Created..")
        })
        .catch(error=>{
            window.alert("Please enter the valid Project Id")
            console.log("error",error)
        })
    }

    render(){
        return (
            <form onSubmit={this.handleOnSubmit}>
                <div className="form-box">
                    <label>Project Id</label>
                    <input type="number" placeholder="Project Id" name="project_id" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Project Name</label>
                    <input type="text" placeholder="Project Name" name="project_name" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Project Description</label>
                    <input type="text" placeholder="Project Description" name="project_description" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box">
                    <label>Project Owner</label>
                    <input type="text" placeholder="Project Owner" name="project_owner" required onChange={this.handleOnChange}/>
                </div>
                
                <div className="form-box"> 
                    <button type="submit" >Create</button>
                </div>
            </form>
        )
    }
}
export default CreateProject