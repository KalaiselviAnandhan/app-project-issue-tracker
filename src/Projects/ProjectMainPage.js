import React from "react"
import {Link} from "react-router-dom"


function ProjectMainPage(){
    return(
        <div className = 'project-container'>
            <Link to = '/'><button value = {0} >Home</button></Link>
            <Link to = '/projects'>
                <button value = {1} >Show All Projects</button>
            </Link>
                <Link to = '/createproject'><button value = {2} >Create Project</button></Link>
        </div>
    )
}
export default ProjectMainPage