import React from "react"
import axios from "axios"
import Tabledata from "./Tabledata"

class ShowAllProjects extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            projects : []
        }
    }

    fetchData = ()=>{
        axios.get("https://kalaianand-project-issue-track.herokuapp.com/projects")
        .then(response => {
            this.setState({
                projects : response.data
            })
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
        this.fetchData()
    }


    handleDelete=(event)=>{
        let projectId = event.target.id
        axios.delete(`https://kalaianand-project-issue-track.herokuapp.com/projects/${projectId}`)
        .then(response => {
            alert("Deleted...")
            this.fetchData();
            console.log(response)
        })
        .catch(error => console.log(error))

    }

    render(){
        return(
            <div>
                <table>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Project Name</td>
                            <td>Project Description</td>
                            <td>Project Owner</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.projects.map((items,index)=><Tabledata projectData = {items} key={index} clickTitle={this.TrackIssue} clickDelete = {this.handleDelete}/>)}
                    </tbody>
                </table>
                
            </div>
        )
        
    }
}
export default ShowAllProjects