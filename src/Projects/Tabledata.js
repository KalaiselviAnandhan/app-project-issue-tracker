import React from "react"
import {Link} from "react-router-dom"

class TableData extends React.Component{
    render(){
        let id=this.props.projectData.project_id
        return(
        <tr>
            <td>
                {this.props.projectData.project_id}
            </td>
            <td>
                <Link to ={`/projects/${id}/issues`}>
                    <p >
                        {this.props.projectData.project_name}
                    </p>
                </Link>
            </td>
            <td>
                {this.props.projectData.project_description}
            </td>
            <td>
                {this.props.projectData.project_owner}
            </td>
            <td>
                <Link to ={`/projects/${id}`}>
                    <input type = "button" value ='edit'/>
                </Link>
            </td>
            <td>
                <input type="button" value="delete" id = {this.props.projectData.project_id} onClick = {(event)=>this.props.clickDelete(event,this.props.projectData)}/>
            </td>
        </tr>
        )
    }
}
export default TableData