import React from 'react'
import axios from "axios"

class TraceProject extends React.Component {
    constructor(props){
        super(props)
        this.state={
            project_name:'',
            project_description:'',
            project_owner:''
        }
    }

    handleChange = (event)=>{
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }
    handleSubmit = (event)=>{
        event.preventDefault()
        let data = this.state
        axios.put(`https://kalaianand-project-issue-track.herokuapp.com/projects/${this.props.match.params.projectId}`,data)
        .then(response=>{
            alert("Project Updated..")
            console.log(response)
        })
        .catch(error=>console.log(error))
    }
    render(){
        
        return (
            <form onSubmit={this.handleSubmit}>
                
                <div className="form-box">
                    <label>Project Name</label>
                    <input type="text" placeholder="Project Name" name="project_name" required onChange = {this.handleChange} />
                </div>
                
                <div className="form-box">
                    <label>Project Description</label>
                    <input type="text" placeholder="Project Description" name="project_description" required onChange={this.handleChange}/>
                </div>
                
                <div className="form-box">
                    <label>Project Owner</label>
                    <input type="text" placeholder="Project Owner" name="project_owner" required onChange={this.handleChange}/>
                </div>

                <div className="form-box"> 
                    <button type="submit" >Submit</button>
                </div>
            </form>
        )
    }
}
export default TraceProject